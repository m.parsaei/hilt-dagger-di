package mp.parsa.hiltwithmitch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.android.scopes.FragmentScoped
import dagger.hilt.android.scopes.ViewScoped
import javax.inject.Inject

@AndroidEntryPoint
class Scopes : AppCompatActivity() {           // ACTIVITY

    @Inject
    lateinit var someClass: SomeClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        someClass.doSomeThing()
    }
}

@AndroidEntryPoint
class MyFragment : Fragment() {               // FRAGMENT
    @Inject
    lateinit var someClass: SomeClass
}

//@FragmentScoped : error for activity injection
//@ViewScoped : error for both activity and fragment injection
@ActivityScoped
class SomeClass
@Inject constructor(){
    fun doSomeThing() = println("Doing some thing!")
}

