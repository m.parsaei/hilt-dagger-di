package mp.parsa.hiltwithmitch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FieldAndConstructorInjection : AppCompatActivity() {

    //field injection
    @Inject
    lateinit var classA: ClassA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        classA.doJobA()
        classA.doOtherJob()
    }
}

class ClassA
//constructor injection
@Inject constructor(private val classB: ClassB){
    fun doJobA() = println("Doing Job A!")

    fun doOtherJob() = classB.doJobB()
}

class ClassB
@Inject constructor(){
    fun doJobB() = println("Doing Job B!")
}