package mp.parsa.hiltwithmitch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.android.scopes.FragmentScoped
import dagger.hilt.android.scopes.ViewScoped
import javax.inject.Inject
import javax.inject.Singleton

@AndroidEntryPoint
class InterfaceAndThirdPartyInjectionIssue : AppCompatActivity() {

    @Inject
    lateinit var myClass: MyClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        myClass.doSomeThing()
    }
}

class MyClass
@Inject constructor(
    //private val someInterfaceImpl:SomeInterfaceImplementation, // (It's ok, but not a good way to implement and test)
    private val someInterfaceImpl:SomeInterface,  // (error: It's ok, but hilt can not solve it)
    private val gson:Gson // (error: we can not modify Gson constructor to be injectable. It's a third party library)
){
    fun doSomeThing() = println("Doing some thing ${someInterfaceImpl.doSomeOtherThing()}")
}

class SomeInterfaceImplementation
@Inject constructor():SomeInterface {
    override fun doSomeOtherThing() = "Doing some other thing ...."
}

interface SomeInterface {
    fun doSomeOtherThing() : String
}

// SOLVING ISSUES

@InstallIn(ActivityComponent::class)
@Module
abstract class MyAbstractModule {

    @ActivityScoped
    @Binds // (Don't need to implementation)
    abstract fun bindSomeInterface (someInterfaceImpl: SomeInterfaceImplementation) :SomeInterface
}

@InstallIn(ApplicationComponent::class)
@Module
class MyModule {

    @Singleton // (ApplicationComponent::class) (@ActivityScoped => ActivityComponent::class)
    @Provides // (if need implementation to instantiate)
    fun provideGson () = Gson()
}